;;;; -*- Mode: Lisp -*-

;;;; new-op-pkg.lisp --
;;;; Replacing MAKE-INSTANCE and other DEFSTRUCT constructors.
;;;;
;;;; Please see file COPYING for copyright and licensing details.

(defpackage "IT.UNIMIB.DISCO.MA.CL.EXT.TAC.NEW-OP" (:use "CL")
 (:nicknames
  "COMMON-LISP.EXTENSIONS.TYPE-AND-CLASSES.NEW-OP"
  "CL.EXT.TAC.NEW-OP"
  "NEW-OP")
 (:export "NEW" "NEWQ")
 (:export "TYPE-CONS")
 (:export "REGISTER-STRUCTURE-CONSTRUCTOR")
 (:export "*COERCE-NUMBERS-P*" "*COERCE-ARRAY-ELEMENTS-P*")
 )


;;;; end of file -- new-op-pkg.lisp --
