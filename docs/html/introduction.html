<!DOCTYPE HTML PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<!-- introduction.html -->

<html>
<head>
<title>NEW op CL</title>
<link rel="stylesheet" href="helambdap.css">
</head>


<body>

<h1><a name="Introduction"></a>NEW: a <tt>new</tt> operator for Common
Lisp</h1>

<p><strong>Common Lisp</strong> is a functional language (and also an
imperative, object-oriented one, which, moreover, can be used in a
declarative fashion) and it has a mode of operation where
&quot;objects&quot; are <em>created</em> on the fly (on the heap, and
then dispatched by the garbage collector).  Object creation is done
through a slew of <em>ad-hoc</em> constructors (think
<code>list</code>, <code>cons</code>, <code>make-array</code> etc.),
constructors tied to a specific <em>structure</em> and
<code>make-instance</code>.</p>

<p>Alas, many &quot;new&quot; programming languages have settled on an
operator that dynamically creates <em>new</em> objects in memory:
<code>new</code>.
</p>

<p>This library (which is nothing new) introduces the <code>new</code>
and <code>newq</code> operators for Common Lisp.  The idea is to
provide an alternative to all (or most) ad-hoc constructors and
<code>make-instance</code>, so that you can write something
like the following:</p>

<pre>
    cl-prompt&gt; <b>(defclass foo () ())</b>
    <i>FOO</i>

    cl-prompt&gt; <b>(<span style="color: red">new</span> 'foo)</b>
    <i>#&lt;FOO 42&gt;</i>
</pre>

or, with a convenience macro

<pre>
    cl-prompt&gt; <b>(<span style="color: red">newq</span> foo)</b>
    <i>#&lt;FOO 42&gt;</i>
</pre>
</p>

<p>Of course, this does not amount to much, yet, things become more
interesting when you try things like the following:</p>

<pre>
    cl-prompt&gt; <b>(<span style="color: red">newq</span> <span
style="color: blue">(array single-float (2 2))</span> '((1.0 0.0) (0.0 1.0)))</b>
    <i>#2A((1.0 0.0) (0.0 1.0))</i>

    cl-prompt&gt; <b>(type-of *)</b>
    <i>(SIMPLE-ARRAY SINGLE-FLOAT (2 2))</i>

    cl-prompt&gt; <b>(<span style="color: red">newq</span> <span
    style="color: blue">(array double-float)</span> '((1.0 0.0) (0.0 1.0))</b>
    <i>Error: Attempt to set double-float array element to 1.0 of type SINGLE-FLOAT.</i>

    cl-prompt> <b>(<span style="color: red">new</span> '<span
    style="color: blue">(mod 42)</span> (read))</b>
    1024
    <i>Error: 1024 is not of type (INTEGER 0 (42)).</i>
</pre>
</p>

<p>The problem is that as soon as you wade in CL "type specifiers"
land, you hit the dreaded "implementation dependent" wall, or, worse.
the "this is not even mentioned in the ANSI spec" wall. Think of
DEFSTRUCT and the fact that there is no portable way to get to the
constructor, not to mention DEFTYPE.</p>



<h1><a name="Specification_Rationale"></a>Specification Rationale</h1>

<p>The rationale for the specification of the <code>new</code>
operator is simply to provide an operator that is familiar to many
programmers and that seamlessly meshes in a regular CL environment.
Ancillary functionalities are provided to make the <code>new</code>
operator work for special (and not so special) cases.</p>

<p>The key and simple invariant that should always hold for the specification,
the <a href="Reference_Implementation">reference implementation</a>
and for any user-provided extension is the following:</p>

<pre>
    cl-prompt&gt; <b>(typep (<span style="color: red">new</span> <span style="color: blue"><em>type</em></span> ...) <span style="color: blue"><em>type</em></span>)</b>
    <i>T</i>
</pre>
</p>


<h2><code>new</code> and <code>newq</code> macros</h2>

<p>The main operator <code>new</code> is a <em>generic function</em>
with several methods defined (at least in the <a
href="Reference_Implementation">reference implementation</a>).
<code>newq</code> is a convenience macro that does not evaluate the
first argument and simply expands in a call to <code>new</code>.</p>


<h2>Defining Methods to Handle <code>deftype</code>-defined Types</h2>

<p>CL types can be defined with <code>deftype</code>.  The result is a
type specifier that is either a symbol, or, most likely, a
<code>cons</code>.  The primary method for <code>new</code>
<code>cons</code> eventually
dispatches on the generic function <code>type-cons</code>, which mostly
discriminates on <code>eql</code> specializers on the head of the type
specifier.
</p>

<h2>Defining Methods to Handle <code>structure-class</code>
Classes</h2>

<p>The main problem that any implementation of the <b>NEW</b> API must
face is how to deal with <code>structure-class</code>es.  The CL
specification does not make provisions for ways to fetch the
constructors of a DEFSTRUCT, which can be many.</p>

<p>The <b>NEW</b> API provides two ways to deal with such a problem:
(1) by introducing a modicum of new "syntax" that can be used to
circumvent the problem, and (2) by providing a way to <em>record</em>
the DEFSTRUCT constructors.

<h3>Extended "syntax" for <code>structure-class</code> and
<code>standard-class</code></h3>

<p>In order to provide a way to specify the "constructor" (and
therefore the standard one) in a call to <b><code>new</code></b> or
<b><code>newq</code></b> for a <code>structure-class</code> the
following syntax is introduced to specify a "pseudo type" clearly
identifying strucutre types (and, simmetrically, class types); in some
circuitous way this is akin to the C++ <code>typename</code> keyword.
<pre>
   '(' <span style="color: red">structure</span> <i>&lt;structure type designator&gt;</i> [ <span style="color: red">by</span> <i>&lt;constructor&gt;</i> ] ')'

   '(' <span style="color: red">class</span> <i>&lt;class designator&gt;</i> [ <span style="color: red">by</span> <i>&lt;constructor&gt;</i> ] ')'
</pre>

<h4>Example</h4>

<p>
<pre>
    cl-prompt&gt; <b>(defstruct foo a s d)</b>
    <i>FOO</i>

    cl-prompt&gt; <b>(new '(<span style="color: red">structure</span> foo) :s 42)</b>
    <i>#S(FOO :A NIL :S 42 :D NIL)</i>

    cl-prompt&gt; <b>(defstruct (bar (:constructor <span style="color: blue">baryfy</span> (a s))
			       (:constructor <span style="color: blue">babar</span> (a &amp;optional (d (+1 a)))))
		   a s d)</b>
    <i>FOO</i>

    cl-prompt&gt; <b>(new '(<span style="color: red">structure</span> bar <span style="color: red">:by</span> <span style="color: blue">baryfy</span>) "QWE" "RTY")</b>
    <i>#S(FOO :A "QWE" :S "RTY" :D NIL)</i>

    cl-prompt&gt; <b>(newq (<span style="color: red">structure</span> bar <span style="color: red">:by</span> <span style="color: blue">babar</span>) 42)</b>
    <i>#S(FOO :A 42 :S NIL :D 43)</i>

    cl-prompt&gt; <b>(defclass baz () ())</b>
    <i>BAZ</i>

    cl-prompt&gt; <b>(newq (<span style="color: red">class</span> BAZ))</b>
    <i>#&lt;BAZ 42&gt;</i>
</pre>


<h3><em>Recording</em> <code>structure-class</code> constructors</h3>

<p>While implementations must have a "generic" structure constructor
to deal with the <code>#S(STRUC-CLASS-NAME ...)</code> syntax, alas,
such functionality is not portably advertised.  Because of this fact,
in order to allow the "plain" use of constructors (i.e., without the
wrapping <code>(structure <i>&lt;s&gt;</i>)</code>),
<code>new</code> must rely on an explicit "recording" of a constructor
for the structure, lest redefining the macro DEFSTRUCT.  In order to
register a constructor for the benefit of <code>new</code> the
function <code>register-structure-constructor</code> is thus
provided.
</p>

<h4>Example</h4>

<p>
<pre>
   cl-prompt&gt; <b>(defstruct foo a s d)</b>
   <i>FOO</i>

   cl-prompt&gt; <b>(<span style="color: red">register-structure-constructor</span> 'foo #'make-foo)</b>
   <i>#&lt;Function MAKE-FOO ...&gt;</i>

   cl-prompt&gt; <b>(new 'foo :s 42)</b>
   <i>#S(FOO :A NIL :S 42 :D NIL)</i>
</pre>

<h1><a name="Downloads and Repositories"></a>Downloads and Repositories</h1>

<p>The <strong>NEW</strong> specification and reference implementation
can be found at
<a href="http://common-lisp.net/" target=_blank>common-lisp.net</a>.
The implementation lies within a package nicknamed <tt>NEW-OP</tt> and
is based on the generic functions <tt>new</tt>, <tt>type-cons</tt>,
and the macro <tt>newq</tt>.
</p>

<p>The reference implementation contains a number of predefined
methods for <tt>new</tt> and <code>type-cons</code> that handle most
of the basic <strong>CL</strong> types and classes.
</p>

<p>The NEW-OP library is available from <a
href="http://www.quicklisp.org" target="_blank">Quicklisp</a>.</p>

<p>The  specification and (reference) implementation
can be found at <a href="http://common-lisp.net"
target="_blank">common-lisp.net</a>.
</p>

<p>
The <a href="http://www.git-scm.com" target="_blank"><tt>git</tt></a>
can be gotten from the
<a href="http://common-lisp.net" target="_blank">common-lisp.net</a>
<a href="http://www.gitlab.com" target="_blank">GitLab</a> instance in the
<a href="https://gitlab.common-lisp.net/new-op/new-op" target="_blank">NEW-OP project page</a>.
</p>


<h1><a name="References"></a>References</h1>

<p>The usual ones.  Plus various past discussions on C.L.L. and
elsewhere.</p>


<h1><a name="Disclaimer"></a>Disclaimer</h1>

<p><em>The code associated to these documents is not
completely tested and it is bound to contain errors and omissions.
This documentation may contain errors and omissions as well.</em></p>


<h1><a name="License"></a>License</h1>

<p>The code is released under a BSD license. See the file
<tt>COPYING</tt> in the code tree. You are advised to use
the code at your own risk.  No warranty whatsoever is provided, the
author will not be held responsible for any effect generated by your
use of the library, and you can put here and around each piece of the
library code the scariest extra disclaimer you can think of.
</p>
</em>

    
  

<!--
;;; Copyright (c) 2010-2016 Marco Antoniotti, All rigths reserved.
;;;
;;; Permission to use, modify, and redistribute this code is hereby
;;; granted.
;;; The code is provided AS IS with NO warranty whatsoever. The author
;;; will not be held liable etc etc etc etc etc.
-->

</body>
</html>

<!-- end of file : introduction.html -->
